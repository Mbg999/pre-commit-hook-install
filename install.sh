#!/bin/bash

# repo -> https://gitlab.com/Mbg99/pre-commit-hook-install

formatter="google-java-format-1.7-all-deps.jar" # formatter file name
precommit="pre-commit" # pre-commit file name
manualformatter="manual-formatter.sh" # manual formatter file name
cyan=`tput setaf 6;`
red=`tput setaf 1;`
white=`tput setaf 7;`
continue="s"
while [[ ${continue:0:1} = "s" || ${continue:0:1} = "S" ]]
do
    clear
    path="🍕🍕🍕🍕"
    formatterPath=""

    # asking for path loop
    while [[ -z $path || ! -d $path ]]
    do
        read  -p "${white}Introduce la ruta donde se encuentren tus proyectos (ej: /c/arqgeaP/src): " path

        if [[ $path && ! -d $path ]]
        then
            echo "${red}No se ha encontrado la ruta 😿"
        fi
    done

    # looking for projects
    echo "${white}Buscando proyectos, por favor, espere... 🔍"
    projects=($(find $path -type d -name ".git")) # look for the .git folder
    clear
    
    if [ $projects ] # if here are some projects
    then
        if [ -z $formatterPath ]
        then
            read -p "${white}Path para el .jar formatter (ej: /c/arqgeaP/src), si ya tienes uno, pulsa enter sin mas: " formatterPath
            if [[ $formatterPath && ! -f "$formatterPath/$formatter" ]] # copy the formatter if needed
            then
                formatterPath=`echo $formatterPath/$formatter`
                echo -e "${cyan}\nformatter copiado en $formatterPath\n"
                cp "./$formatter" $path
                # set the formatter path into the formatter variable of the pre-commit script
                line=`cat $precommit | grep -n "formatter=" | cut -d ":" -f 1` # get the line number
                sed -e "${line}s:.*:formatter=\"$formatterPath\":" -i $precommit # replace the line with the new formatter path
                # set the formatter path into the formatter variable of the manual-formatter script
                line=`cat $manualformatter | grep -n "formatter=" | cut -d ":" -f 1` # get the line number
                sed -e "${line}s:.*:formatter=\"$formatterPath\":" -i $manualformatter # replace the line with the new formatter path
            fi
        fi

        echo "${cyan}"
        j=0
        for p in "${projects[@]}" # loop over the projects and copy the pre-commit into the hooks folder
        do
            #if [ ! -f "$p/hooks/pre-commit" ] # uncomment in case you don't want to override the file, feel free of improve this part
            #then
                echo "hook copiado en $p/hooks/pre-commit"
                cp "./$precommit" "$p/hooks/pre-commit"
                ((j++))
            #fi
            
        done
        echo -e "${white}\n--------- DONE 🐱‍👍 ${j} proyectos actualizados ---------"
    else # no projects in that path
        echo "${red}no hay proyectos en ese directorio"
        path="🍕🍕🍕🍕"
    fi
    
    # ask if you need to add more projects
    continue=""
    while [ -z $continue ]
    do
        read  -p "${white}tienes proyectos en otras rutas? (si/no): " continue
    done

done

echo "salu2 🐱"

formatter="google-java-format-1.7-all-deps.jar" # formatter file name, place your path here
cyan=`tput setaf 6;`
red=`tput setaf 1;`
white=`tput setaf 7;`
continue="y"

while [[ ${continue:0:1} = "y" || ${continue:0:1} = "S" ]]
do
    clear
    path="🍕🍕🍕🍕"

    # asking for path loop
    while [[ -z $path || ! -d $path ]]
    do
        read  -p "${white}Insert a path for your project (ex: /c/arqgeaP/src/eReceta/erecetaback): " path

        if [[ $path && ! -d $path ]]
        then
            echo "${red}Can't find that path 😿"
        fi
    done

    # looking for files
    echo "${white}Looking for the project, please, wait... 🔍"
    cd $path # move to the project path
    files=($(git diff --name-only '*.java')) # look for the uncommited files
    clear
    if [ $files ] # if there are files
    then
        total=0
        f=0
        err=0
        for file in "${files[@]}" #loop over the files
        do
            echo "${cyan}$file${red}"
            if ! java -jar $formatter --replace $file # format the file
            then
                echo "-- ERROR --"
                ((err++))
            else
                ((f++))
            fi
            ((total++))
        done
        echo -e "${white} ---------\nDONE:\n\t - total files: $total\n\t - formated files: ${f}\n\t - files with error: ${err}\n---------"
    else
        echo "${red}There are no .java files in this project"
    fi
    # ask if you need to add more projects
    continue=""
    while [ -z $continue ]
    do
        read  -p "${white}Do you have any other project to format? (yes/no): " continue
    done
done

echo "salu2 🐱"
# pre-commit-hook-install

<h3>Just run the install.sh in a git bash shield like ./install.sh, you need to have the Google formatter and pre-commit file in the same directory</h3>

<h5>More about the pre-hook script at: https://gitlab.com/Mbg99/format-at-pre-commit-hook</h5>

<p>Format all the java files of a project: https://gitlab.com/Mbg99/format-projects</p>

<p>me: https://github.com/Mbg999</p>
